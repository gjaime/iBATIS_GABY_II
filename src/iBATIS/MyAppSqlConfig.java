package iBATIS;
import java.io.Reader;

import com.ibatis.common.resources.*;
import com.ibatis.sqlmap.client.*;

public class MyAppSqlConfig {
	//public MyAppSqlConfig {
	
	//SqlMapClient sqlMap = MyAppSqlConfig.getSqlMapInstance();
	
		private static final SqlMapClient sqlMap;
		static {
		try {
		String resource = "iBATIS/SqlMapConfig.xml";
		Reader reader = Resources.getResourceAsReader (resource);
		sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {
			
		e.printStackTrace();
		throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
		}
		public static SqlMapClient getSqlMapInstance () {
		return sqlMap;
		}		
}
