package iBATIS;

import java.util.List;

public interface PapasHijos {

	/**
	 * Obtiene un listado de todos los padres y la cantidad de hijos
	 * @return Lista de objetos del tipo <PadreQtyHijos>
	 * @since 28/04/2016
	 */
	List<PadreQtyHijos> getPadresQtyHijos();
	List<PadreQtyHijos> getPadreMyrHijos();
	
}
