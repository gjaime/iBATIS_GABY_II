package iBATIS;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.ibatis.sqlmap.client.SqlMapClient;

public class Principal2 {
	
	static final Logger logger = LogManager.getLogManager(Principal2.class);
	
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		
		/*para que retorne un listado de objetos que corresponda 
		al listado de padres y la cantidad de hijos*/
		SqlMapClient sqlMap = MyAppSqlConfig.getSqlMapInstance();
		
		//1) Se necesita obtener un listado de Padres y la cantidad de hijos
		List<PadreQtyHijos> lista = (List<PadreQtyHijos>) sqlMap.queryForList("getPapasHijos");
		/*for ( PadreQtyHijos dto : lista )
			System.out.println("nombre="+dto.getNombre());*/
		PadreQtyHijos dto;
		for (int i=0; i<lista.size(); i++ ) {
			dto = (PadreQtyHijos)lista.get(i);
			logger.debug("Nombre del pap�: "+dto.getNombre());
			logger.debug("Cantidad de hijos: " + dto.getHijos());
			System.out.println("Nombre del pap�: "+dto.getNombre());
			System.out.println("Cantidad de hijos: " + dto.getHijos());
		}
		
		//2) Se necesita obtener el listado de Padres con hijos mayores de edad
		List<PadreQtyHijos> lista2 = (List<PadreQtyHijos>) sqlMap.queryForList("getPapasHijosMayores");
			for (int b=00; b<=lista2.size(); b++){
				dto = (PadreQtyHijos)lista2.get(b);
				System.out.println(dto.getNombre());
				logger.debug(dto.getNombre());
			}
		
		// 3) Hijos que son padres 
		List<PadreQtyHijos> lista3 = (List<PadreQtyHijos>) sqlMap.queryForList("getHijosQueSonPadres");
		for (int c=0; c<lista3.size(); c++){
			dto = (PadreQtyHijos)lista3.get(c);
			System.out.println(dto.getNombre());
			logger.debug(dto.getNombre());
		}
		
		//4)Se necesita obtener un listado de Padres que no tienen hijos
		List<PadreQtyHijos> lista4 = (List<PadreQtyHijos>) sqlMap.queryForList("getPapasSinHijos");
		for (int a=0; a<=lista4.size(); a++){
			dto = (PadreQtyHijos)lista4.get(a);
			logger.debug(dto.getNombre());
			System.out.println(dto.getNombre());
		}
		
	}

}
